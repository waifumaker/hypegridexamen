<?php

$db = new PDO("mysql:dbname=fito;host=localhost", "fito", "bluee");
$cmd  = $_REQUEST['cmd'];

/**
* 
*/

function UUID_a_id_estudiante($uuid)
{
  global $db;
  $stmt = $db->prepare('SELECT * FROM Academia_estudiantes WHERE uuid_opensim=?');
  $stmt->bindParam(1,$uuid ,PDO::PARAM_STR);
  $stmt->execute();
  $res = $stmt->fetchAll();
  if (count($res) == 0 )
  {
    return false;
  }
  return $res[0]['id_estudiante'];

}

function registrar_UUID()
{
  global $_REQUEST; 
  global $db;
  $req = $_REQUEST; 
  $stmt = $db->prepare('UPDATE Academia_estudiantes SET uuid_opensim=? ' .
          ',nombre_opensim=? WHERE nombre_real=? AND '. 
          ' apellido_real=?');
  $stmt->bindParam(1,$_REQUEST['uuid'] ,PDO::PARAM_STR);
  $_REQUEST['iname']=strtoupper($_REQUEST['iname']);
  $stmt->bindParam(2,$_REQUEST['iname'],PDO::PARAM_STR);
  $_REQUEST['rname']=strtoupper($_REQUEST['rname']);
  $stmt->bindParam(3,$_REQUEST['rname'],PDO::PARAM_STR);
  $_REQUEST['rlast']=strtoupper($_REQUEST['rlast']);
  $stmt->bindParam(4,$_REQUEST['rlast'],PDO::PARAM_STR);
  $stmt->execute();
}

function leer_cuestionario($cuestionario)
{
   global $db;

   $query =  
     'SELECT Academia_preguntas.texto AS pregunta, '                                       .
     'Academia_opciones_preguntas.texto AS texto_opciones,'                                .
     'Academia_preguntas.opcion_correcta,'                                                 .
     'Academia_opciones_preguntas.* FROM Academia_preguntas,Academia_opciones_preguntas,'  .
     'Academia_cuestionarios WHERE Academia_opciones_preguntas.id_pregunta '               .
     '= Academia_preguntas.id_pregunta AND Academia_preguntas.id_cuestionario '            .
     '=Academia_cuestionarios.id_cuestionario AND Academia_cuestionarios.id_cuestionario=?'.
     ' group by Academia_opciones_preguntas.id_opcion order by Academia_opciones_preguntas.id_pregunta ASC';
   $stmt = $db->prepare($query);
   if ($stmt===false) 
   {
     echo "here";
     print_r($db->errorInfo());
   }
   $stmt->bindParam(1,$cuestionario);
   $stmt->execute();

   $res = $stmt->fetchAll();
   $quid = 0;
   $rc = count($res);
   for( $c=0 ; $rc != $c; $c++)
   {
      if ($res[$c]['id_pregunta'] != $quid)
      {
        echo ":QSTART:\n";
        echo $res[$c]['pregunta']."\n";
        echo $res[$c]['id_pregunta']."\n";
        echo $res[$c]['opcion_correcta']."\n";
      }
      $quid = $res[$c]['id_pregunta'];
      echo $res[$c]['texto_opciones']."\n";
   }
   //print_r($res);
   /*$rc = count($res);
   echo $res[$n_pregunta]['pregunta']."\n"; 
   echo $res[$n_pregunta]['texto_opciones']."\n";
   echo $res[$n_pregunta]['etiqueta']."\n";*/
}

function enviar_cuestionario($uuid,$cuestionario,$respuestas,$id_preguntas)
{
   global $db;
   $rl = strlen($respuestas);
   $ids= preg_split('/-/', $id_preguntas);
   $udbid =  UUID_a_id_estudiante($uuid);
   for($c=0; $c!= $rl; $c++)
   {
      $stmt = $db->prepare('INSERT INTO Academia_respuestas VALUES(NULL,?,?,?,NULL)');
      $stmt->bindParam(1,$ids[$c] ,PDO::PARAM_INT);
      $stmt->bindParam(2,$udbid,PDO::PARAM_INT);
      $ca=$respuestas[$c];
      $stmt->bindParam(3,$ca,PDO::PARAM_STR);
      $stmt->execute();
   }
   exit('ok');

}

function finalizar_examen()
{

}

if ($cmd == "usuario_registrado")
{
   $dbid = UUID_a_id_estudiante($_REQUEST['uuid']);
   if ($dbid === false)
   {
     exit('error');
   }
   else
   {
     exit("ok");
   }

}

if ($cmd == "actualizar_uuid")
{
   registrar_UUID();
   exit('ok');
}

if ($cmd == "leer_cuestionario")
{
   leer_cuestionario($_REQUEST['cuestionario']);
}

if ($cmd == "enviar_respuestas")
{
   enviar_cuestionario($_REQUEST['uuid'],
                       $_REQUEST['cuestionario'],
                       $_REQUEST['respuestas'],
                       $_REQUEST['id-preguntas']);
}
//obtener_pregunta(2,1)

?>
