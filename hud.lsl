integer cListener;    
key http_request_id;
key http_user_exists; 
key http_finish_test;
key http_update_uuid; 

list questions = [];
string options;
list correct_answers   = [];
string question_ids      = "";
list user_answers = [];
integer current_question = 0;
integer testEnded = FALSE; 
integer nQuestions = 0; 
integer channel = 73;
integer test_id = 1;

string baseURL = "http://lemomo.duckdns.org/~fito/quiz_hud/controller.php?cmd=";

nextQuestion()
{
  if (current_question >= nQuestions)
  {
      return;
  }
  list ops =llParseString2List(options, [":::"],[]);
  string n0 = llList2String(ops,current_question);
  list subOpts = llParseString2List(n0, ["~~~"],[]);
  integer c;
  integer cl = llGetListLength(subOpts);
  llOwnerSay(llList2String(questions,current_question));
  list buttons =[];
  for(c=0; c!= cl; c++)
  {
    string astr = llList2String(subOpts,c);
    llOwnerSay(astr);
    buttons +=llGetSubString(astr,0,0);
  }
  //llSleep(1);
  //llDialog( llDetectedKey(0), llList2String(questions,current_question) ,buttons, channel );
}

plotResults()
{
    
}
float calculateResult()
{
    integer c;
    float correct = 0;
    for(c=0; c!= nQuestions; c++)
    {
        if (llList2String(correct_answers,c) == llList2String(user_answers,c))
         correct++;
    }
    return  correct/(float)nQuestions;
    //return correct;
}

default
{
   
    state_entry()
    {
        testEnded = FALSE;
        cListener = llListen(channel, "",llGetOwner(), "");
        http_user_exists=  llHTTPRequest(baseURL + "usuario_registrado&uuid="+(string)llGetOwner(), [], "");
        llSetText( "Examen en progreso", <0,0,0>, 1 );
        llSetPrimitiveParams([PRIM_TYPE, 
                            PRIM_TYPE_CYLINDER, 
                            PRIM_HOLE_DEFAULT,  // hole_shape
                            <0.00, 1, 0.0>,   // cut
                            0.0,                // hollow
                            <0.0, 0.0, 0.0>,    // twist
                            <1.0, 1.0, 0.0>,    // top_size
                            <0.0, 0.0, 0.0>     // top_Shear
        ]);      
    }
 
    http_response(key request_id, integer status, list metadata, string body)
    {
       if (request_id == http_update_uuid )
       {
           if ( body != "ok")
           {
            llOwnerSay("ocurrio un error dando de alta el usuario");
            llOwnerSay("debug:"+body);
           }
           return;
       }
       if (request_id == http_user_exists){ 
         llOwnerSay(body);
         if (body != "ok")
         {
             llOwnerSay("el usuario no se encuentra registrado en el sistema de examenes");
             llOwnerSay("porfavor registrese usando /"+(string)channel + "alta nombre apellido");
             return;
         }
         http_request_id = llHTTPRequest(baseURL + "leer_cuestionario&cuestionario="+(string)test_id, [], "");
         llOwnerSay("el examen ha sido descargado correctamente");
         return; 
       }
       if (request_id == http_finish_test){ 
         if (body!= "ok")
         {
             llOwnerSay("ocurrio un error enviando las respuestas");
         }
         return; 
       } 
       //llOwnerSay(body);
       list split  = llParseString2List(body,["\n"],[""]);
       integer lc = llGetListLength(split);
       integer c;
       string cline; 
       list opts =[];
      
       for(c=0; c != lc; c ++ )
       {
          cline = llList2String(split,c);
          if (cline == ":QSTART:")
          {
              c++; 
              questions +=   llList2String(split,c);
              c++;
              question_ids += llList2String(split,c) +"-";
              c++;
              correct_answers += llList2String(split,c);
              user_answers += "";
              options += ":::";
              nQuestions++;
          }
          else 
          {
              options += cline + "~~~";
          }
       }
       //llOwnerSay((string)split);
    }
    touch_start(integer total_number)
    {
      nextQuestion();
    }
    
    listen(integer chan, string name, key id, string msg)
    {
       msg = llToUpper(msg);
       if (testEnded )
       {
          llOwnerSay("El examen ha terminado");
          
          return;
       }
       if (llStringLength(msg) > 5 )
       {
        
           string alta = llGetSubString( msg,  0 , 3 );
           if (alta == "alta")
           {
               list nl = llParseString2List(msg, [" "],[]);
               integer c = llGetListLength(nl);
               integer c2;
               string realname; 
               string lastnames; 
               if (c < 3)
               {
                   llOwnerSay("porfavor envie nombre y apellido  ");
               }else
               {
                   realname = llList2String(nl,1);
                   for(c2=2; c2 != c; c2++)
                   {
                       lastnames +=  llList2String(nl,c2); 
                       if (c2+1 < c) lastnames+=" ";  
                   }
                   http_update_uuid =llHTTPRequest(baseURL +  "actualizar_uuid&uuid="+(string)llGetOwner()+"&iname="+llGetUsername(llGetOwner())+
                                 "&rname=" + realname + "&rlast=" + lastnames , [], "");
                   /*llOwnerSay(   baseURL +  "actualizar_uuid&uuid="+(string)llGetOwner()+"&iname="+llGetUsername(llGetOwner())+
                                 "&rname=" + realname + "&rlast=" + lastnames);   */
                  
                   llOwnerSay("verificando registro"); 
                   http_user_exists=  llHTTPRequest(baseURL + "usuario_registrado&uuid="+(string)llGetOwner(), [], "");
               }
               return;
           }
       }
       if (msg == "ATRAS")
       {
           current_question--;
           if (current_question < 0 ) current_question = 0; 
       }
       else if (msg == "SIGUIENTE")
       {
           current_question++;
           if ( current_question >= nQuestions ) 
           {
               current_question = nQuestions;
               llOwnerSay("Haz alcanzando el final del exámen");
           }
       }
       else if (msg == "TERMINAR")
       {
           http_finish_test =  llHTTPRequest(baseURL + "enviar_respuestas&uuid="+(string)llGetOwner()+
                                 "&cuestionario="+(string)test_id + "&respuestas="+(string)user_answers+
                                 "&id-preguntas="+question_ids
                                , [], "");
           /*llOwnerSay(baseURL + "enviar_respuestas&uuid="+(string)llGetOwner()+
                                 "&cuestionario="+(string)test_id + "&respuestas="+(string)user_answers+
                             "&id-preguntas="+question_ids);*/
            llSetPrimitiveParams([PRIM_TYPE, 
                            PRIM_TYPE_CYLINDER, 
                            PRIM_HOLE_DEFAULT,  // hole_shape
                            <0.00, calculateResult(), 0.0>,   // cut
                            0.0,                // hollow
                            <0.0, 0.0, 0.0>,    // twist
                            <1.0, 1.0, 0.0>,    // top_size
                            <0.0, 0.0, 0.0>     // top_Shear
            ]);      
       }
       else 
       {
           if (current_question >=nQuestions) return;
           user_answers =llListReplaceList(user_answers,[msg],current_question,current_question);
           current_question++;
           if (current_question >=nQuestions )
           {
              llOwnerSay("ha alcanzando el final del examen, escriba /"+(string)channel + " terminar   para enviar sus resultados");   
              llOwnerSay((string)user_answers);
           }else 
           {
               nextQuestion();
           } 
    
       }
      
      
    }

}
