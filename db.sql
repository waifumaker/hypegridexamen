-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 21, 2017 at 02:46 AM
-- Server version: 10.1.23-MariaDB
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fito`
--

-- --------------------------------------------------------

--
-- Table structure for table `Academia_cuestionarios`
--

CREATE TABLE `Academia_cuestionarios` (
  `id_cuestionario` int(11) NOT NULL,
  `descripcion` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `id_tema` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `Academia_cuestionarios`
--

INSERT INTO `Academia_cuestionarios` (`id_cuestionario`, `descripcion`, `id_tema`) VALUES
(1, 'CUESTIONARIO DE PRUEBA', 1),
(2, 'SEGUNDO CUESTIONARIO DE PRUEBA', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Academia_estudiantes`
--

CREATE TABLE `Academia_estudiantes` (
  `id_estudiante` int(11) NOT NULL,
  `uuid_opensim` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre_real` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `apellido_real` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nombre_opensim` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='nombre_opensim';

--
-- Dumping data for table `Academia_estudiantes`
--

INSERT INTO `Academia_estudiantes` (`id_estudiante`, `uuid_opensim`, `nombre_real`, `apellido_real`, `nombre_opensim`) VALUES
(1, '2d821414-5b61-4923-9f8c-e476a16bf707', 'ESTUDIANTE', 'PRUEBA', 'METUS.VERSIKUS');

-- --------------------------------------------------------

--
-- Table structure for table `Academia_materias`
--

CREATE TABLE `Academia_materias` (
  `id_materia` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `Academia_materias`
--

INSERT INTO `Academia_materias` (`id_materia`, `nombre`) VALUES
(1, 'MATERIA DE RUEBA');

-- --------------------------------------------------------

--
-- Table structure for table `Academia_opciones_preguntas`
--

CREATE TABLE `Academia_opciones_preguntas` (
  `id_pregunta` int(11) NOT NULL,
  `id_opcion` int(11) NOT NULL,
  `texto` varchar(1024) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `Academia_opciones_preguntas`
--

INSERT INTO `Academia_opciones_preguntas` (`id_pregunta`, `id_opcion`, `texto`) VALUES
(1, 1, 'a) rojo'),
(1, 2, 'b) azul'),
(1, 3, 'c) verde'),
(2, 4, 'a) copiar un archivo'),
(2, 5, 'b) eliminar un archivo'),
(2, 6, 'c) reiniciar el sistema'),
(3, 7, 'a) 8'),
(3, 8, 'b) 16'),
(3, 9, 'c) 4'),
(4, 10, 'a) TRANMISSION CONTROL PROTOCOL'),
(4, 11, 'b) TRANSFER CONTROL PROTOCOL'),
(4, 12, 'c) TRANSMISSION CENTRAL PROTOCOL');

-- --------------------------------------------------------

--
-- Table structure for table `Academia_preguntas`
--

CREATE TABLE `Academia_preguntas` (
  `id_pregunta` int(11) NOT NULL,
  `texto` varchar(1024) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `opcion_correcta` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `activa` int(1) NOT NULL,
  `id_cuestionario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `Academia_preguntas`
--

INSERT INTO `Academia_preguntas` (`id_pregunta`, `texto`, `opcion_correcta`, `activa`, `id_cuestionario`) VALUES
(1, 'en RGB de (0 a 1 ), ¿cólor seria 1,0,0', 'A', 1, 1),
(2, 'la herramienta rm sirve para:', 'B', 1, 1),
(3, '¿cuántos bits tiene un nibble?', 'C', 1, 2),
(4, 'tcp significa ', 'A', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `Academia_profesores`
--

CREATE TABLE `Academia_profesores` (
  `id_profesor` int(11) NOT NULL,
  `Apellido` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Nombre` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Academia_respuestas`
--

CREATE TABLE `Academia_respuestas` (
  `id_respuesta` int(11) NOT NULL,
  `id_pregunta` int(11) NOT NULL,
  `id_estudiante` int(11) NOT NULL,
  `opcion_elegida` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_respuesta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `Academia_respuestas`
--

INSERT INTO `Academia_respuestas` (`id_respuesta`, `id_pregunta`, `id_estudiante`, `opcion_elegida`, `fecha_respuesta`) VALUES
(4, 0, 1, 'a', '2017-05-21 00:47:11'),
(5, 0, 1, 'a', '2017-05-21 00:48:51'),
(6, 1, 1, 'a', '2017-05-21 01:26:11'),
(7, 2, 1, 'b', '2017-05-21 01:26:11'),
(8, 1, 1, 'A', '2017-05-21 02:03:07'),
(9, 2, 1, 'B', '2017-05-21 02:03:07'),
(10, 1, 1, 'A', '2017-05-21 02:33:01'),
(11, 2, 1, 'B', '2017-05-21 02:33:01'),
(12, 1, 1, 'A', '2017-05-21 02:34:11'),
(13, 2, 1, 'B', '2017-05-21 02:34:11'),
(14, 1, 1, 'A', '2017-05-21 02:34:16'),
(15, 2, 1, 'B', '2017-05-21 02:34:16'),
(16, 1, 1, 'A', '2017-05-21 02:34:23'),
(17, 2, 1, 'B', '2017-05-21 02:34:23'),
(18, 1, 1, 'A', '2017-05-21 02:34:46'),
(19, 2, 1, 'B', '2017-05-21 02:34:46'),
(20, 0, 1, 'A', '2017-05-21 02:34:46'),
(21, 1, 1, 'A', '2017-05-21 02:35:38'),
(22, 2, 1, 'B', '2017-05-21 02:35:38'),
(23, 1, 1, 'A', '2017-05-21 02:37:43'),
(24, 2, 1, 'B', '2017-05-21 02:37:43'),
(25, 1, 1, 'C', '2017-05-21 02:38:11'),
(26, 2, 1, 'A', '2017-05-21 02:38:11');

-- --------------------------------------------------------

--
-- Table structure for table `Academia_temas`
--

CREATE TABLE `Academia_temas` (
  `id_tema` int(11) NOT NULL,
  `descripcion` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `id_materia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `Academia_temas`
--

INSERT INTO `Academia_temas` (`id_tema`, `descripcion`, `id_materia`) VALUES
(1, 'ESTA ES UN TEMA DE PRUEBA', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Academia_cuestionarios`
--
ALTER TABLE `Academia_cuestionarios`
  ADD PRIMARY KEY (`id_cuestionario`);

--
-- Indexes for table `Academia_estudiantes`
--
ALTER TABLE `Academia_estudiantes`
  ADD PRIMARY KEY (`id_estudiante`);

--
-- Indexes for table `Academia_materias`
--
ALTER TABLE `Academia_materias`
  ADD PRIMARY KEY (`id_materia`);

--
-- Indexes for table `Academia_opciones_preguntas`
--
ALTER TABLE `Academia_opciones_preguntas`
  ADD PRIMARY KEY (`id_opcion`);

--
-- Indexes for table `Academia_preguntas`
--
ALTER TABLE `Academia_preguntas`
  ADD PRIMARY KEY (`id_pregunta`);

--
-- Indexes for table `Academia_profesores`
--
ALTER TABLE `Academia_profesores`
  ADD PRIMARY KEY (`id_profesor`);

--
-- Indexes for table `Academia_respuestas`
--
ALTER TABLE `Academia_respuestas`
  ADD PRIMARY KEY (`id_respuesta`);

--
-- Indexes for table `Academia_temas`
--
ALTER TABLE `Academia_temas`
  ADD PRIMARY KEY (`id_tema`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Academia_cuestionarios`
--
ALTER TABLE `Academia_cuestionarios`
  MODIFY `id_cuestionario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `Academia_estudiantes`
--
ALTER TABLE `Academia_estudiantes`
  MODIFY `id_estudiante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `Academia_materias`
--
ALTER TABLE `Academia_materias`
  MODIFY `id_materia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `Academia_opciones_preguntas`
--
ALTER TABLE `Academia_opciones_preguntas`
  MODIFY `id_opcion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `Academia_preguntas`
--
ALTER TABLE `Academia_preguntas`
  MODIFY `id_pregunta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `Academia_respuestas`
--
ALTER TABLE `Academia_respuestas`
  MODIFY `id_respuesta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `Academia_temas`
--
ALTER TABLE `Academia_temas`
  MODIFY `id_tema` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
